<?php 


/**
 *
 * Címkezelő osztály - http://schema.org/PostalAddress típus alapján
 *
 */

class Address {

  protected $_id; //Egyedi azonosító
  protected $_created; //Létrehozás dátuma (Unix időbélyeg)
  protected $_updated; //Módosítás dátuma (Unix időbélyeg)
  public $postalCode; //Irányítószám
  public $familyName; //Családnév
  public $givenName; //Keresztnév
  public $streetAddress; //Utca és házszám
  public $addressLocality; //Város
  public $addressRegion; //Régió, megye
  public $addressCountry; //Ország


  public function __construct($data=[]) {

    $this->_created = time();
  
    if(is_array($data)){
      foreach($data as $name => $value) { $this->$name = $value; }
    } else { trigger_error('Nem nyert'); }

  }


  public function __get($name){

    $protected_name = '_'.$name;
    if(property_exists($this, $protected_name)){
      return $this->$protected_name;
    }

    if($name == "postalCode") {
      $this->postalCode = self::get_postalCode('../data/postalcodes.csv', $this->addressLocality);
    }

    trigger_error("Nem létező vagy védett tulajdonságot próbálsz elérni (__get):  $name");

  }


  public function __set($name, $value) {

    if($name == "postalCode") {
      $this->postalCode = self::get_postalCode('../data/postalcodes.csv', $this->addressLocality);
    }

    trigger_error("Nem létező vagy védett tulajdonságot próbálsz elérni (__set): $name | $value");

  }


  public static function autocomplete_locality($datafile, $input) {

    $file = fopen($datafile, 'r');
    while (($line = fgetcsv($file, 100, ',')) !== FALSE) { $places[$line[0]] = $line[1]; }
    fclose($file);

    $postalcodes = preg_grep("/^".ucfirst($input).".*/", $places);

    if(!empty($postalcodes)) {
      $result = "";
      foreach($postalcodes as $locality) {
        $result .= "\n\t\t\t\t\t<option value=\"$locality\">$locality</option>";
      }
      $result .= "\n\t\t\t\t";
    } else { $result = "Nincs javaslat..."; }

    return $result;

  }


  public static function get_postalCode($datafile, $locality) {
  
    $file = fopen($datafile, 'r');
    while (($line = fgetcsv($file, 100, ',')) !== FALSE) { $places[$line[0]] = $line[1]; }
    fclose($file);

    $result = array_search($locality, $places);
    return $result;
  
  }


  public function __toString(){

    return "\n    <h1>Kedves {$this->familyName} {$this->givenName}!</h1>\n    <p>Leveledet megkaptuk, a fekete löttyöt a következő címre továbbítjuk neked:</p>\n    <p><b>{$this->addressLocality}</b></p>\n    <p>{$this->streetAddress}</p>\n    <p>{$this->postalCode}</p><br>\n    <p><i>Üdvözlettel: Alien Medical Kft.</i></p>\n  ";

  }

}

?>
