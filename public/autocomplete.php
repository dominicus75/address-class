<?php

spl_autoload_register(function($class) { include("../classes/$class.class.php"); } );

if(filter_input(INPUT_POST, 'keyword')) {
  $response = Address::autocomplete_locality('../data/postalcodes.csv', trim($_POST['keyword']));
} elseif(filter_input(INPUT_POST, 'locality')) {
  $response = Address::get_postalCode('../data/postalcodes.csv', trim($_POST['locality']));
}

echo $response;

?>
